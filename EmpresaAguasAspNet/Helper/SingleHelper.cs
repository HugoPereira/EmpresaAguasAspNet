﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EmpresaAguasAspNet.Models;
using Microsoft.AspNet.Identity;

namespace EmpresaAguasAspNet.Helper
{
    public class SingleHelper: IDisposable
    {
        private static ApplicationDbContext db = new ApplicationDbContext();

        private static EmpresaAguasAspNetContext dc = new EmpresaAguasAspNetContext();

        /// <summary>
        /// encontra um consumo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Consumption GetConsumptionById(int id)
        {
            Consumption consumption = dc.Consumptions.FirstOrDefault(x => x.ConsumptionId == id);

            return consumption;
        }

        /// <summary>
        /// retorna um cliente
        /// </summary>
        /// <param name="userManager"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Client GetClient(UserManager<ApplicationUser> userManager, string id)
        {
            // Lista de Users existentes
            var user = userManager.Users.ToList().Find(u => u.Id == id);

            var userView = new Client
            {
                Email = user.Email,
                UserName = user.UserName,
                ClientId = user.Id,
                UserType = user.UserType,
                Adress = user.Adress,
                Location = user.Location,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Tin = user.Tin
            };

            return userView;
        }


        /// <summary>
        /// Fechar conecção à base de dados
        /// </summary>
        public void Dispose()
        {
            db.Dispose();
            dc.Dispose();
        }
    }
}