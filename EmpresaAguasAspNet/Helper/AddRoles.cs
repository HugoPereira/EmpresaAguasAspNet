﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EmpresaAguasAspNet.Models;
using Microsoft.AspNet.Identity;

namespace EmpresaAguasAspNet.Helper
{
    public class AddRoles
    {
        /// <summary>
        /// atribuir as roles
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userManager"></param>
        public static void AddingRoles(string userId, UserManager<ApplicationUser> userManager)
        {
            var users = userManager.Users.ToList();
            var user = users.Find(u => u.Id == userId);

            // Não existe utilizador
            if (user == null)
            {
                return;
            }

            // Permissões para Clientes
            if (user.UserType == UserType.Cliente.ToString())
            {
                if (!userManager.IsInRole(user.Id, "User"))
                {
                    userManager.AddToRole(user.Id, "User");
                }

                if (!userManager.IsInRole(user.Id, "Client"))
                {
                    userManager.AddToRole(user.Id, "Client");
                }

                if (!userManager.IsInRole(user.Id, "Client Edit"))
                {
                    userManager.AddToRole(user.Id, "Client Edit");
                }

                if (!userManager.IsInRole(user.Id, "Consumption"))
                {
                    userManager.AddToRole(user.Id, "Consumption");
                }

                if (!userManager.IsInRole(user.Id, "Consumption View"))
                {
                    userManager.AddToRole(user.Id, "Consumption View");
                }


                if (!userManager.IsInRole(user.Id, "Consumption Create"))
                {
                    userManager.AddToRole(user.Id, "Consumption Create");
                }

                if (!userManager.IsInRole(user.Id, "Consumption Edit"))
                {
                    userManager.AddToRole(user.Id, "Consumption Edit");
                }


                if (!userManager.IsInRole(user.Id, "Consumption Delete"))
                {
                    userManager.AddToRole(user.Id, "Consumption Delete");
                }

                if (!userManager.IsInRole(user.Id, "Invoices"))
                {
                    userManager.AddToRole(user.Id, "Invoices");
                }

                if (!userManager.IsInRole(user.Id, "Invoices View"))
                {
                    userManager.AddToRole(user.Id, "Invoices View");
                }


            }

            // Permissões para Administradores
            if (user.UserType == UserType.Administrador.ToString())
            {
                // Verificar se já tem as roles e adicionar
                // Roles Clientes
                if (!userManager.IsInRole(user.Id, "Admin"))
                {
                    userManager.AddToRole(user.Id, "Admin");
                }


                if (!userManager.IsInRole(user.Id, "Client"))
                {
                    userManager.AddToRole(user.Id, "Client");
                }

                if (!userManager.IsInRole(user.Id, "Client View"))
                {
                    userManager.AddToRole(user.Id, "Client View");
                }


                if (!userManager.IsInRole(user.Id, "Client Edit"))
                {
                    userManager.AddToRole(user.Id, "Client Edit");
                }


                // Roles Consumos
                if (!userManager.IsInRole(user.Id, "Consumption"))
                {
                    userManager.AddToRole(user.Id, "Consumption");
                }

                if (!userManager.IsInRole(user.Id, "Consumption View"))
                {
                    userManager.AddToRole(user.Id, "Consumption View");
                }

                if (!userManager.IsInRole(user.Id, "Consumption Create"))
                {
                    userManager.AddToRole(user.Id, "Consumption Create");
                }

                if (!userManager.IsInRole(user.Id, "Consumption Edit"))
                {
                    userManager.AddToRole(user.Id, "Consumption Edit");
                }

                if (!userManager.IsInRole(user.Id, "Consumption Delete"))
                {
                    userManager.AddToRole(user.Id, "Consumption Delete");
                }

                // Roles Escaloes
                if (!userManager.IsInRole(user.Id, "Levels"))
                {
                    userManager.AddToRole(user.Id, "Levels");
                }

                if (!userManager.IsInRole(user.Id, "Levels View"))
                {
                    userManager.AddToRole(user.Id, "Levels View");
                }

                if (!userManager.IsInRole(user.Id, "Levels Create"))
                {
                    userManager.AddToRole(user.Id, "Levels Create");
                }

                if (!userManager.IsInRole(user.Id, "Levels Edit"))
                {
                    userManager.AddToRole(user.Id, "Levels Edit");
                }

                if (!userManager.IsInRole(user.Id, "Levels Delete"))
                {
                    userManager.AddToRole(user.Id, "Levels Delete");
                }

                // Roles Faturas
                if (!userManager.IsInRole(user.Id, "Invoices"))
                {
                    userManager.AddToRole(user.Id, "Invoices");
                }

                if (!userManager.IsInRole(user.Id, "Invoices View"))
                {
                    userManager.AddToRole(user.Id, "Invoices View");
                }

                if (!userManager.IsInRole(user.Id, "Invoices Create"))
                {
                    userManager.AddToRole(user.Id, "Invoices Create");
                }
            }
        }

    }
}