﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EmpresaAguasAspNet.Models;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EmpresaAguasAspNet.Helper
{
    public class CombosHelper : IDisposable
    {
        private static ApplicationDbContext db = new ApplicationDbContext();

        private static EmpresaAguasAspNetContext dc = new EmpresaAguasAspNetContext();

        /// <summary>
        /// cria a lista dos utilizadores paea acrregar na combobox
        /// </summary>
        /// <param name="userManager"></param>
        /// <returns></returns>
        public static List<Client> ListUsers(UserManager<ApplicationUser> userManager)
        {
            // Lista de Users existentes
            var users = userManager.Users.Where(x => x.UserType == UserType.Cliente.ToString()).ToList();

            // Lista do tipo UserView
            var usersView = new List<Client>();

            // Passar para a View
            foreach (var item in users)
            {

                var userView = new Client
                {
                    Email = item.Email,
                    UserName = item.UserName,
                    ClientId = item.Id,
                    UserType = item.UserType,
                    Adress = item.Adress,
                    Location = item.Location,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    Tin = item.Tin,
                    Name = $"{item.FirstName} {item.LastName}"

                };

                usersView.Add(userView);
            }

            usersView.Add(new Client
            {
                ClientId = 0.ToString(),
                UserName = "[Selecione um cliente]"
            });

            return usersView.OrderBy(u => u.ClientId).ToList();

        }


        /// <summary>
        /// lista os clientes
        /// </summary>
        /// <param name="userManager"></param>
        /// <returns></returns>
        public static List<Client> ListClient(UserManager<ApplicationUser> userManager)
        {
            // Lista de Users existentes
            var users = userManager.Users.Where(x => x.UserType == UserType.Cliente.ToString()).ToList();

            // Lista do tipo UserView
            var usersView = new List<Client>();

            // Passar para a View
            foreach (var item in users)
            {

                var userView = new Client
                {
                    Email = item.Email,
                    UserName = item.UserName,
                    ClientId = item.Id,
                    UserType = item.UserType,
                    Adress = item.Adress,
                    Location = item.Location,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    Tin = item.Tin,
                    Name = $"{item.FirstName} {item.LastName}"

                };

                usersView.Add(userView);
            }

            return usersView.ToList();

        }

        /// <summary>
        /// lista de consumos
        /// </summary>
        /// <param name="consumptions"></param>
        /// <returns></returns>
        public static List<Consumption> GetListConsumptions(List<Consumption> consumptions)
        {
            // Lista do tipo UserView
            var consumptionList = new List<Consumption>();

            // Passar para a View
            foreach (var item in consumptions)
            {
                string name = db.Users.Find(item.ClientId).UserName;

                var consump = new Consumption
                {
                    ConsumptionId = item.ConsumptionId,
                    ConsumptionCount = item.ConsumptionCount,
                    ConsumptionDate = item.ConsumptionDate,
                    ClientId = item.ClientId,
                    UserClient = name
                };
                consumptionList.Add(consump);

            }

            return consumptionList.ToList();
        }

        /// <summary>
        /// retorna consumos por cliente
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static List<Consumption> GetListConsumptionsById(List<Consumption> consumption, string id)
        {
            var consumptions = consumption.Where(x => x.ClientId == id).ToList();

            // Lista do tipo UserView
            var consumptionList = new List<Consumption>();

            // Passar para a View
            foreach (var item in consumptions)
            {
                string name = db.Users.Find(item.ClientId).UserName;

                var consump = new Consumption
                {
                    ConsumptionId = item.ConsumptionId,
                    ConsumptionCount = item.ConsumptionCount,
                    ConsumptionDate = item.ConsumptionDate,
                    ClientId = item.ClientId,
                    UserClient = name
                };
                consumptionList.Add(consump);

            }

            return consumptionList.ToList();
        }


        /// <summary>
        /// lista dos consumos do cliente ainda nao faturados
        /// </summary>
        /// <param name="consumption"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static List<Consumption> GetListConsumptionsNotInvoiceById(List<Consumption> consumption, string id)
        {
            var consumptions = consumption.Where(x => x.ClientId == id).ToList();

            var list = dc.Invoices.ToList();

            //lista de faturas atribuidas
            var invoiceslist = list.Select(x => x.ConsumptionId).ToList();

            //lista de consumos nao atribuidas
            IEnumerable<Consumption> except = consumptions.Where(x => !invoiceslist.Contains(x.ConsumptionId));

            // Lista do tipo UserView
            var consumptionList = new List<Consumption>();

            // Passar para a View
            foreach (var item in except)
            {
                string name = db.Users.Find(item.ClientId).UserName;

                var consump = new Consumption
                {
                    ConsumptionId = item.ConsumptionId,
                    ConsumptionCount = item.ConsumptionCount,
                    ConsumptionDate = item.ConsumptionDate,
                    ClientId = item.ClientId,
                    UserClient = name
                };
                consumptionList.Add(consump);

            }

            return consumptionList.ToList();
        }


        /// <summary>
        /// retorna a lista dos consumos
        /// </summary>
        /// <param name="invoices"></param>
        /// <returns></returns>
        public static List<Invoice> GetListInvoices(List<Invoice> invoices)
        {
            // Lista do tipo UserView
            var invoiceList = new List<Invoice>();

            // Passar para a View
            foreach (var item in invoices)
            {
                string name = db.Users.Find(item.ClientId).UserName;
                int count = dc.Consumptions.Find(item.ConsumptionId).ConsumptionCount;

                var invoice = new Invoice
                {
                    InvoiceId = item.InvoiceId,
                    InvoiceVat = item.InvoiceVat,
                    InvoiceDate = item.InvoiceDate,
                    ClientId = item.ClientId,
                    UserClient = name,
                    ConsumptionId = item.ConsumptionId,
                    ConsumptionCount = count,
                    InvoiceSubTotal = item.InvoiceSubTotal,
                    InvoiceTotal = item.InvoiceTotal
                };
                invoiceList.Add(invoice);

            }

            return invoiceList.ToList();
        }

        /// <summary>
        /// retorna a lista de faturas de um cliente
        /// </summary>
        /// <param name="invoice"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static List<Invoice> GetListInvoicessById(List<Invoice> invoice, string id)
        {
            var invoices = invoice.Where(x => x.ClientId == id).ToList();

            // Lista do tipo UserView
            var invoicesList = new List<Invoice>();

            // Passar para a View
            foreach (var item in invoices)
            {
                string name = db.Users.Find(item.ClientId).UserName;
                int count = dc.Consumptions.Find(item.ConsumptionId).ConsumptionCount;

                var newInvoice = new Invoice
                {
                    InvoiceId = item.InvoiceId,
                    InvoiceVat = item.InvoiceVat,
                    InvoiceDate = item.InvoiceDate,
                    ClientId = item.ClientId,
                    UserClient = name,
                    ConsumptionId = item.ConsumptionId,
                    ConsumptionCount = count,
                    InvoiceSubTotal = item.InvoiceSubTotal,
                    InvoiceTotal = item.InvoiceTotal
                };
                invoicesList.Add(newInvoice);

            }

            return invoicesList.ToList();
        }

        /// <summary>
        /// lista de consumos nao atribuidos ordenado por clientes
        /// </summary>
        /// <returns></returns>
        public static List<Invoice> ListConsumptionNotInvoice(List<Invoice> invoices)
        {
            var list = dc.Consumptions.ToList();

            //lista de faturas atribuidas
            var invoiceslist = invoices.Select(x => x.ConsumptionId).ToList();

            //lista de consumos nao atribuidas
            IEnumerable<Consumption> except = list.Where(x => !invoiceslist.Contains(x.ConsumptionId));

            var consumptionNotAssign = new List<Invoice>();

            foreach (var item in except)
            {
                string name = db.Users.Find(item.ClientId).UserName;

                var consump = new Invoice
                {
                    ConsumptionId = item.ConsumptionId,
                    ConsumptionCount = item.ConsumptionCount,
                    ClientId = item.ClientId,
                    UserClient = name
                };
                consumptionNotAssign.Add(consump);
            }

            return consumptionNotAssign.DistinctBy(x => x.ClientId).OrderBy(x => x.UserClient).ToList();
        }
        

        /// <summary>
        /// pesquisa de faturas pelo username, pela caixa de pesquisa
        /// </summary>
        /// <param name="invoices"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        public static List<Invoice> GetListInvoicesContains(List<Invoice> invoices, string search)
        {
            // Lista do tipo UserView
            var invoiceList = new List<Invoice>();

            // Passar para a View
            foreach (var item in invoices)
            {
                string name = db.Users.Find(item.ClientId).UserName;
                int count = dc.Consumptions.Find(item.ConsumptionId).ConsumptionCount;

                var invoice = new Invoice
                {
                    InvoiceId = item.InvoiceId,
                    InvoiceVat = item.InvoiceVat,
                    InvoiceDate = item.InvoiceDate,
                    ClientId = item.ClientId,
                    UserClient = name,
                    ConsumptionId = item.ConsumptionId,
                    ConsumptionCount = count,
                    InvoiceSubTotal = item.InvoiceSubTotal,
                    InvoiceTotal = item.InvoiceTotal
                };
                invoiceList.Add(invoice);

            }

            var list = new List<Invoice>();

            list = invoiceList.Where(s => s.UserClient.Contains(search)).ToList();

            return list;
        }

        /// <summary>
        /// pesquisa de consumos pelo username, pela caixa de pesquisa
        /// </summary>
        /// <param name="consumptions"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        public static List<Consumption> GetListConsumptionsContains(List<Consumption> consumptions, string search)
        {
            // Lista do tipo UserView
            var consumptionList = new List<Consumption>();

            // Passar para a View
            foreach (var item in consumptions)
            {
                string name = db.Users.Find(item.ClientId).UserName;

                var consump = new Consumption
                {
                    ConsumptionId = item.ConsumptionId,
                    ConsumptionCount = item.ConsumptionCount,
                    ConsumptionDate = item.ConsumptionDate,
                    ClientId = item.ClientId,
                    UserClient = name
                };
                consumptionList.Add(consump);

            }

            var list = new List<Consumption>();

            list = consumptionList.Where(s => s.UserClient.Contains(search)).ToList();

            return list;
        }


        /// <summary>
        /// Fechar conecção à base de dados
        /// </summary>
        public void Dispose()
        {
            db.Dispose();
            dc.Dispose();
        }
    }
}