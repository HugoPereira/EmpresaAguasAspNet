﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EmpresaAguasAspNet.Startup))]
namespace EmpresaAguasAspNet
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
