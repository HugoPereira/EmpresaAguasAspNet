﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EmpresaAguasAspNet.Models
{
    public class Invoice
    {
        [Key]
        [Display(Name = "Nº Fatura")]
        public int InvoiceId { get; set; }

        [Display(Name = "Data")]
        [DataType(DataType.Date)]
        public DateTime InvoiceDate { get; set; }

        [Display(Name = "IVA")]
        [Required(ErrorMessage = "Tem que inserir o valor do {0}")]
        public decimal InvoiceVat { get; set; }

        [Display(Name = "SubTotal")]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        public decimal InvoiceSubTotal { get; set; }

        [Display(Name = "Total")]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        public decimal InvoiceTotal { get; set; }

        [Display(Name = "Cliente")]
        public string ClientId { get; set; }
        
        [Display(Name = "Consumo")]
        public int ConsumptionId { get; set; }
        
        public virtual Consumption Consumption { get; set; }

        [NotMapped]
        [Display(Name = "Username")]
        public string UserClient { get; set; }

        [NotMapped]
        [Display(Name = "Contagem")]
        public int ConsumptionCount { get; set; }
    }
}