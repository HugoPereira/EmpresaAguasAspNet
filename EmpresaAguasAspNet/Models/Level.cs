﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EmpresaAguasAspNet.Models
{
    public class Level
    {
        [Key]
        public int LevelId { get; set; }

        [Display(Name = "Minimo")]
        [Required(ErrorMessage = "Tem que inserir um {0} para o Escalão")]
        public int LevelMin { get; set; }

        [Display(Name = "Valor Unitário")]
        [Required(ErrorMessage = "Tem que inserir um {0} para o Escalão")]
        public float UnitValue { get; set; }
    }
}