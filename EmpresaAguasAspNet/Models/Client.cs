﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EmpresaAguasAspNet.Models
{
    public class Client
    {
        public string ClientId { get; set; }

        [Display(Name = "Nome")]
        public string FirstName { get; set; }

        [Display(Name = "Apelido")]
        public string LastName { get; set; }

        [Display(Name = "Morada")]
        public string Adress { get; set; }

        [Display(Name = "Localidade")]
        public string Location { get; set; }

        [Display(Name = "NIF")]
        public string Tin { get; set; }


        public string Email { get; set; }

        [Display(Name = "Tipo")]
        public string UserType { get; set; }

        [Display(Name = "Nome Completo")]
        public string Name { get; set; }

        [Display(Name = "Username")]
        public string UserName { get; set; }

    }
}