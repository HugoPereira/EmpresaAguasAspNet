﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EmpresaAguasAspNet.Models
{
    public class Consumption
    {
        [Key]
        public int ConsumptionId { get; set; }

        [Display(Name = "Data")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Inserir a {0} do consumo")]
        public DateTime ConsumptionDate { get; set; }

        [Display(Name = "Contagem")]
        [Required(ErrorMessage = "Inserir a {0}")]
        public int ConsumptionCount { get; set; }

        [Display(Name = "Cliente")]
        public string ClientId { get; set; }

        [NotMapped]
        [Display(Name = "Username")]
        public string UserClient { get; set; }

        [NotMapped]
        [Display(Name = "Contagem")]
        public string Count { get; set; }


        public virtual ICollection<Invoice> Invoices { get; set; }
    }
}