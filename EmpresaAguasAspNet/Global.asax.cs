﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using EmpresaAguasAspNet.Helper;
using EmpresaAguasAspNet.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EmpresaAguasAspNet
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<Models.EmpresaAguasAspNetContext, Migrations.Configuration>());

            ApplicationDbContext db = new ApplicationDbContext();

            CreateAdmin(db);

            //criar os papeis para os users
            CreateRoles(db);

            AddPermitionsToAdmin(db);

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        /// <summary>
        /// dar as permissoes ao administrador
        /// </summary>
        /// <param name="db"></param>
        private void AddPermitionsToAdmin(ApplicationDbContext db)
        {
            // Objeto para manipular user
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            var user = userManager.FindByName("admin@empagua.com");

            AddRoles.AddingRoles(user.Id, userManager);
        }

        /// <summary>
        /// criar o administrador
        /// </summary>
        /// <param name="db"></param>
        private void CreateAdmin(ApplicationDbContext db)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            var user = userManager.FindByName("admin@empagua.com");

            if (user == null)
            {
                user = new ApplicationUser
                {
                    UserName = "admin@empagua.com",
                    Email = "admin@empagua.com",
                    UserType = UserType.Administrador.ToString()

                };

                //password para o user
                userManager.Create(user, "P@ssw0rd");
            }
        }



        private void CreateRoles(ApplicationDbContext db)
        {
            // Objeto para manipular as Roles
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            // Roles Clientes
            if (!roleManager.RoleExists("Client"))
            {
                roleManager.Create(new IdentityRole("Client"));
            }

            if (!roleManager.RoleExists("Client View"))
            {
                roleManager.Create(new IdentityRole("Client View"));
            }
            

            if (!roleManager.RoleExists("Client Edit"))
            {
                roleManager.Create(new IdentityRole("Client Edit"));
            }
            
            // Roles Consumos
            if (!roleManager.RoleExists("Consumption"))
            {
                roleManager.Create(new IdentityRole("Consumption"));
            }

            if (!roleManager.RoleExists("Consumption View"))
            {
                roleManager.Create(new IdentityRole("Consumption View"));
            }

            if (!roleManager.RoleExists("Consumption Create"))
            {
                roleManager.Create(new IdentityRole("Consumption Create"));
            }

            if (!roleManager.RoleExists("Consumption Edit"))
            {
                roleManager.Create(new IdentityRole("Consumption Edit"));
            }

            if (!roleManager.RoleExists("Consumption Delete"))
            {
                roleManager.Create(new IdentityRole("Consumption Delete"));
            }

            // Roles Escaloes
            if (!roleManager.RoleExists("Levels"))
            {
                roleManager.Create(new IdentityRole("Levels"));
            }

            if (!roleManager.RoleExists("Levels View"))
            {
                roleManager.Create(new IdentityRole("Levels View"));
            }

            if (!roleManager.RoleExists("Levels Create"))
            {
                roleManager.Create(new IdentityRole("Levels Create"));
            }

            if (!roleManager.RoleExists("Levels Edit"))
            {
                roleManager.Create(new IdentityRole("Levels Edit"));
            }

            if (!roleManager.RoleExists("Levels Delete"))
            {
                roleManager.Create(new IdentityRole("Levels Delete"));
            }

            // Roles Faturas
            if (!roleManager.RoleExists("Invoices"))
            {
                roleManager.Create(new IdentityRole("Invoices"));
            }

            if (!roleManager.RoleExists("Invoices View"))
            {
                roleManager.Create(new IdentityRole("Invoices View"));
            }

            if (!roleManager.RoleExists("Invoices Create"))
            {
                roleManager.Create(new IdentityRole("Invoices Create"));
            }

            if (!roleManager.RoleExists("Admin"))
            {
                roleManager.Create(new IdentityRole("Admin"));
            }

            if (!roleManager.RoleExists("User"))
            {
                roleManager.Create(new IdentityRole("User"));
            }

        }

    }
}
