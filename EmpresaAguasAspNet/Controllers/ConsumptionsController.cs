﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EmpresaAguasAspNet.Helper;
using EmpresaAguasAspNet.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EmpresaAguasAspNet.Controllers
{
    [Authorize(Roles = "Consumption")]
    public class ConsumptionsController : Controller
    {
        private EmpresaAguasAspNetContext db = new EmpresaAguasAspNetContext();

        private ApplicationDbContext dba = new ApplicationDbContext();


        // GET: Consumptions
        [Authorize(Roles = "Consumption View")]
        public ActionResult Index(string search)
        {
            //apanha o id do user
            string userId = User.Identity.GetUserId();
            
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dba));

            //encontra o utilizador
            var user = userManager.Users.ToList().Find(u => u.Id == userId);

            //lista os consumos
            var consumptions = db.Consumptions.ToList();

            List<Consumption> list = new List<Consumption>();

            //dependendo o tipo de user retorna uma lista
            if (user.UserType == UserType.Administrador.ToString())
            {
                list = CombosHelper.GetListConsumptions(consumptions).ToList();
            }
            else
            {
                list = CombosHelper.GetListConsumptionsById(consumptions, userId).ToList();
            }

            //caso a string nao esteja vazia
            if (!string.IsNullOrEmpty(search))
            {
                if (user.UserType == UserType.Administrador.ToString())
                {
                    list = CombosHelper.GetListConsumptionsContains(consumptions, search).ToList();
                }

            }

            return View(list);
        }



        // GET: Consumptions/Create
        [Authorize(Roles = "Consumption Create")]
        public ActionResult Create()
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dba));
            //retorna os clientes mas mostra o username
            ViewBag.ClientId = new SelectList(CombosHelper.ListUsers(userManager), "ClientId", "UserName");
           
            return View();
        }

        // POST: Consumptions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ConsumptionId,ConsumptionDate,ConsumptionCount,ClientId")] Consumption consumption)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dba));

            //guarda o utilizador
            string userId = User.Identity.GetUserId();

            var user = userManager.Users.ToList().Find(u => u.Id == userId);

            // Verifica o user se foi escolhido
            if (consumption.ClientId == "0")
            {
                ViewBag.ErrorClient = "Tem que selecionar um Cliente";

                ViewBag.ClientId = new SelectList(CombosHelper.ListUsers(userManager), "ClientId", "UserName");

                return View(consumption);
            }

            //verifica se ja existe alguma contagem
            var existsConsumption = db.Consumptions.FirstOrDefault(x =>
                x.ConsumptionDate.Month == consumption.ConsumptionDate.Month &&
                x.ConsumptionDate.Year == consumption.ConsumptionDate.Year && x.ClientId == consumption.ClientId);

            // Verifica se ja foi inserido um consumo neste mês, para este cliente
            if (existsConsumption != null)
            {
                ViewBag.Error = "Já inseriu uma contagem neste mês!";
                ViewBag.ClientId = new SelectList(CombosHelper.ListUsers(userManager), "ClientId", "UserName");
                return View(consumption);
            }

            //verifica se a data é anterior a que esta na lista
            var existsConsumptionDate = db.Consumptions.FirstOrDefault(x =>
                x.ConsumptionDate > consumption.ConsumptionDate && x.ClientId == consumption.ClientId);

            // Verifica se está a inserir um registo anterior ao ultimo registo
            if (existsConsumptionDate != null)
            {
                ViewBag.Error = "Não pode inserir uma contagem anterior às contagens inseridas";
                ViewBag.ClientId = new SelectList(CombosHelper.ListUsers(userManager), "ClientId", "UserName");
                return View(consumption);
            }
            
            if (ModelState.IsValid)
            {
                try
                {
                    db.Consumptions.Add(consumption);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                
            }

            return View(consumption);
        }

        // GET: Consumptions/Edit/5
        [Authorize(Roles = "Consumption Edit")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consumption consumption = db.Consumptions.Find(id);

            if (consumption == null)
            {
                return HttpNotFound();
            }

            //procura este consumo nas faturas
            var existsInInvoice = db.Invoices.FirstOrDefault(x => x.ConsumptionId == id);

            //se existir algum
            if (existsInInvoice != null)
            {
                TempData["Error"] = "Este consumo já foi Faturado. Não pode ser alterado.";
                return RedirectToAction("Index");
            }
            
            return View(consumption);
            
        }

        // POST: Consumptions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ConsumptionId,ConsumptionDate,ConsumptionCount,ClientId")] Consumption consumption)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dba));

            if (consumption == null)
            {
                return HttpNotFound();
            }

            //verifica se foi feita alguma alteracao
            var exists = db.Consumptions.FirstOrDefault(x =>
                x.ConsumptionDate == consumption.ConsumptionDate &&
                x.ConsumptionCount == consumption.ConsumptionCount &&
                x.ClientId == consumption.ClientId);

            // Verifica se ja foi inserido um consumo neste mês, para este cliente
            if (exists != null)
            {
                ViewBag.Error = "Não efectuou nenhuma alteração!";
                ViewBag.ClientId = new SelectList(CombosHelper.ListUsers(userManager), "ClientId", "UserName");
                return View(consumption);
            }
            
            //verifica se foi alterada a data
            var existsConsumption = db.Consumptions.FirstOrDefault(x =>
                x.ConsumptionDate != consumption.ConsumptionDate &&
                x.ConsumptionCount == consumption.ConsumptionCount &&
                x.ClientId == consumption.ClientId);

            // Verifica se ja foi inserido um consumo neste mês, para este cliente
            if (existsConsumption != null)
            {
                ViewBag.Error = "Não pode alterar a data. Apenas pode alterar a contagem!";
                ViewBag.ClientId = new SelectList(CombosHelper.ListUsers(userManager), "ClientId", "UserName");
                return View(consumption);
            }
            
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(consumption).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    return HttpNotFound();
                }
            }

            return View(consumption);
        }

        // GET: Consumptions/Delete/5
        [Authorize(Roles = "Consumption Delete")]
        public ActionResult Delete(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consumption consumption = db.Consumptions.Find(id);
            if (consumption == null)
            {
                return HttpNotFound();
            }

            //procura este consumo nas faturas
            var existsInInvoice = db.Invoices.FirstOrDefault(x => x.ConsumptionId == id);

            //se existir algum faturado nao apaga
            if (existsInInvoice != null)
            {
                TempData["Error"] = "Este consumo já foi Faturado. Não pode ser eliminado.";
                return RedirectToAction("Index");
            }


            return View(consumption);
        }

        // POST: Consumptions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Consumption consumption = db.Consumptions.Find(id);

            if (consumption == null)
            {
                return HttpNotFound();
            }

            //procura este consumo nas faturas
            var existsInInvoice = db.Invoices.FirstOrDefault(x => x.ConsumptionId == id);

            //se existir algum
            if (existsInInvoice != null)
            {
                TempData["Error"] = "Este consumo já foi Faturado. Não pode ser eliminado.";
                return RedirectToAction("Index");

            }

            try
            {
                db.Consumptions.Remove(consumption);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return HttpNotFound();
            }
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
