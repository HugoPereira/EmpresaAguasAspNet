﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EmpresaAguasAspNet.Models;

namespace EmpresaAguasAspNet.Controllers
{
    [Authorize(Roles = "Levels")]
    public class LevelsController : Controller
    {
        private EmpresaAguasAspNetContext db = new EmpresaAguasAspNetContext();

        // GET: Levels
        [Authorize(Roles = "Levels View")]
        public ActionResult Index()
        {
            return View(db.Levels.ToList());
        }


        // GET: Levels/Create
        [Authorize(Roles = "Levels Create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Levels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LevelId,LevelMin,UnitValue")] Level level)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Levels.Add(level);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                
            }

            return View(level);
        }

        // GET: Levels/Edit/5
        [Authorize(Roles = "Levels Edit")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Level level = db.Levels.Find(id);
            if (level == null)
            {
                return HttpNotFound();
            }
            return View(level);
        }

        // POST: Levels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LevelId,LevelMin,UnitValue")] Level level)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(level).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    return HttpNotFound();
                }
                
            }
            return View(level);
        }

        // GET: Levels/Delete/5
        [Authorize(Roles = "Levels Delete")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Level level = db.Levels.Find(id);
            if (level == null)
            {
                return HttpNotFound();
            }
            return View(level);
        }

        // POST: Levels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            
            Level level = db.Levels.Find(id);

            if (level == null)
            {
                return HttpNotFound();
            }

            try
            {
                db.Levels.Remove(level);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return HttpNotFound();
            }
           
        }

        /// <summary>
        /// fecha a conexao
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
