﻿namespace EmpresaAguasAspNet.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;
    using Helper;
    using Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using iTextSharp.text.pdf;

    [Authorize(Roles = "Invoices")]
    public class InvoicesController : Controller
    {
        private EmpresaAguasAspNetContext db = new EmpresaAguasAspNetContext();

        private ApplicationDbContext dba = new ApplicationDbContext();

        // GET: Invoices
        [Authorize(Roles = "Invoices View")]
        public ActionResult Index(string search)
        {
            //apanha o id do user
            string userId = User.Identity.GetUserId();

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dba));
            //encontra o user
            var user = userManager.Users.ToList().Find(u => u.Id == userId);

            //lista de faturas
            var invoices = db.Invoices.ToList();

            List<Invoice> list = new List<Invoice>();

            //se for o admin a ver
            if (user.UserType == UserType.Administrador.ToString())
            {
                list = CombosHelper.GetListInvoices(invoices).ToList();
            }
            else
            {
                //se for o cliente
                list = CombosHelper.GetListInvoicessById(invoices, userId).ToList();
            }

            
            //caso a string nao esteja vazia
            if (!string.IsNullOrEmpty(search))
            {
                if (user.UserType == UserType.Administrador.ToString())
                {
                    list = CombosHelper.GetListInvoicesContains(invoices, search).ToList();
                }
               
            }

            return View(list);
        }

        // GET: Invoices/Details/5
        [Authorize(Roles = "Invoices View")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }

        // GET: Invoices/Create
        [Authorize(Roles = "Invoices Create")]
        public ActionResult Create(string id)
        {
            var consumptions = db.Consumptions.ToList();
            //envia a lista de consumos
            ViewBag.ConsumptionId = new SelectList(CombosHelper.GetListConsumptionsNotInvoiceById(consumptions, id), "ConsumptionId", "ConsumptionCount");
            return View();
        }

        // POST: Invoices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "InvoiceId,InvoiceDate,InvoiceVat,InvoiceTotal,InvoiceSubTotal,ClientId,ConsumptionId")] Invoice invoice, string id)
        {
            var consumptions = db.Consumptions.ToList();
            
            //verifica se escolheu um consumo
            if (invoice.ConsumptionId == 0)
            {
                ViewBag.ErrorConsumption = "Tem que selecionar um Consumo";

                ViewBag.ConsumptionId = new SelectList(CombosHelper.GetListConsumptionsNotInvoiceById(consumptions, id), "ConsumptionId", "ConsumptionCount");

                return View(invoice);
            }


            //atribui dados a fatura
            int count = db.Consumptions.Find(invoice.ConsumptionId).ConsumptionCount;

            //insere o id
            invoice.ClientId = id;

            //atribui o iva
            invoice.InvoiceVat = 23;

            //mete a data de hj
            invoice.InvoiceDate = DateTime.Now;

            //faz as contas pelos escaloes
            invoice.InvoiceSubTotal = Convert.ToDecimal(SubTotal(count));
            invoice.InvoiceTotal = Convert.ToDecimal(SubTotal(count)) + ((invoice.InvoiceSubTotal * invoice.InvoiceVat) / 100);
            
            if (ModelState.IsValid)
            {
                try
                {
                    db.Invoices.Add(invoice);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                
            }
            
            return View(invoice);
        }


        [Authorize(Roles = "Invoices Create")]
        public ActionResult SelectUser()
        {
            var invoices = db.Invoices.ToList();

            return View(CombosHelper.ListConsumptionNotInvoice(invoices));
        }

        /// <summary>
        /// faz os calcumos do consumos peos escaloes
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        private double SubTotal(int count)
        {
            // Obter a lista de todos os escaloes onde entra a contagem

            List<Level> levelsList = db.Levels.Where(x => x.LevelMin < count).OrderBy(x => x.LevelMin).ToList();

            double subTotal = 0;
            int levelcount = 0;
            
            // ciclo para calcular o subtotal
            for (int i = 0; i < levelsList.Count; i++)
            {
                // se for o ultimo idx da lista
                if (i == levelsList.Count - 1)
                {
                    subTotal += levelsList[i].UnitValue * count;
                }
                else
                {
                    // Calculo do subtotal:
                    // 1 - Vai buscar o valorUnitario do idx
                    // 2 - Vai buscar o valor minimo do escalao a seguir e retira 1
                    // 3 - Ao ponto 2 retiramos a contagem que já foi contabilizada
                    // 4 - Multiplica-se o ponto 1 com o ponto 3
                    subTotal += levelsList[i].UnitValue * (levelsList[i + 1].LevelMin - 1 - levelcount);
                    count -= levelsList[i + 1].LevelMin - 1 - levelcount;
                    levelcount = levelsList[i + 1].LevelMin - 1;
                }
            }

            return subTotal;
        }


        /// <summary>
        /// imprime as faturas dos clientes
        /// </summary>
        /// <param name="id"></param>
        public void Print(int? id)
        {
            var details = db.Invoices.Where(x => x.InvoiceId == id);

            var det = db.Invoices.FirstOrDefault(x => x.InvoiceId == id);

            string client = details.FirstOrDefault(x => x.InvoiceId == id).ClientId;

            var clients = dba.Users.FirstOrDefault(x => x.Id == client);
            
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-disposition", "attachment; filename=fatura.pdf");

            //pega no template
            PdfReader pdfReader = new PdfReader(Request.MapPath("~/Content/pdf/template/template.pdf"));
            PdfStamper pdfStamper = new PdfStamper(pdfReader, Response.OutputStream);
            pdfStamper.FormFlattening = true; // generate a flat PDF
            AcroFields pdfForm = pdfStamper.AcroFields;
            pdfForm.SetField("nfatura", det.InvoiceId.ToString());

            pdfForm.SetField("nome", clients.FirstName + " " + clients.LastName);
            
            pdfForm.SetField("morada", clients.Adress);

            pdfForm.SetField("nif", clients.Tin);

            pdfForm.SetField("localidade", clients.Location);

            pdfForm.SetField("iva", det.InvoiceVat.ToString());

            pdfForm.SetField("subtotal", det.InvoiceSubTotal.ToString());

            pdfForm.SetField("data", det.InvoiceDate.ToShortDateString());

            pdfForm.SetField("total", det.InvoiceTotal.ToString());
            pdfStamper.Close();
            
        }

        /// <summary>
        /// fecha a conexao da bd
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
                dba.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
