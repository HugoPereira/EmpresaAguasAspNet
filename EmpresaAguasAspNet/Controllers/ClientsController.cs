﻿using EmpresaAguasAspNet.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EmpresaAguasAspNet.Helper;

namespace EmpresaAguasAspNet.Controllers
{
    [Authorize(Roles = "Client")]
    public class ClientsController : Controller
    {
        private EmpresaAguasAspNetContext db = new EmpresaAguasAspNetContext();

        private ApplicationDbContext dba = new ApplicationDbContext();

        [Authorize(Roles = "Client View")]
        public ActionResult Index()
        {
            //lista de clientes
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dba));
            return View (CombosHelper.ListClient(userManager));
        }

        // GET: Users/Details/5
        [Authorize(Roles = "Client View")]
        public ActionResult Details(string userId)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dba));

            //se estiver vazio
            if (string.IsNullOrEmpty(userId))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //se for nulo
            if (SingleHelper.GetClient(userManager, userId) == null)
            {
                return HttpNotFound();
            }

            return View(SingleHelper.GetClient(userManager, userId));

        }

        // GET: Products/Edit/5
        [Authorize(Roles = "Client Edit")]
        public ActionResult Edit(string userId)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dba));
            //se estiver vazio
            if (string.IsNullOrEmpty(userId))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (SingleHelper.GetClient(userManager, userId) == null)
            {
                return HttpNotFound();
            }
            //retorna um cliente
            return View(SingleHelper.GetClient(userManager, userId));

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Client model)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dba));

            //se for nulo
            if (model == null)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    ApplicationUser user = userManager.FindById(model.ClientId);
                    
                    user.Adress = model.Adress;
                    user.Location = model.Location;
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.Tin = model.Tin;

                    userManager.Update(user);
                    
                    return RedirectToAction("Index","Home");
                     
                }
                catch (Exception e)
                {
                    return HttpNotFound();
                }

            }
            return View(model);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dba.Dispose();
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}