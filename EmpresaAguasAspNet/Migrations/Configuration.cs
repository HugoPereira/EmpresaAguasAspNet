namespace EmpresaAguasAspNet.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<EmpresaAguasAspNet.Models.EmpresaAguasAspNetContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "EmpresaAguasAspNet.Models.EmpresaAguasAspNetContext";
        }

        protected override void Seed(EmpresaAguasAspNet.Models.EmpresaAguasAspNetContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
